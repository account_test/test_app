package daemon

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"flag"

	"../db"
	"../model"
	"../rest"
)

type Config struct {
	ListenSpec string

	Db db.Config
}

func ProcessFlags() *Config {
	cfg := &Config{}

	flag.StringVar(&cfg.ListenSpec, "listen", "0.0.0.0:3000", "HTTP app port")
	flag.StringVar(&cfg.Db.ConnectString, "db-connect", "host=192.168.99.100 port=5432 user=postgres"+
		" password=password dbname=testdb sslmode=disable", "DB Connect String")

	flag.Parse()
	return cfg
}

func GetModel(cfg *Config) *model.Model {
	db, err := db.InitDb(cfg.Db)
	if err != nil {
		log.Printf("Error initializing database: %v\n", err)
		return nil
	}

	m := model.New(db)
	return m
}

func Run(cfg *Config) error {
	log.Printf("Starting, HTTP on: %s\n", cfg.ListenSpec)

	m := GetModel(cfg)

	rest.Start(m, cfg.ListenSpec)

	waitForSignal()

	return nil
}

func waitForSignal() {
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	s := <-ch
	log.Printf("Got signal: %v, exiting.", s)
}
