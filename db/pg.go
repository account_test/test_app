package db

import (
	"database/sql"

	_ "github.com/lib/pq"
)

type Config struct {
	ConnectString string
}

func InitDb(cfg Config) (*pgDb, error) {
	if dbConn, err := sql.Open("postgres", cfg.ConnectString); err != nil {
		return nil, err
	} else {
		p := &pgDb{dbConn: dbConn}
		if err := p.dbConn.Ping(); err != nil {
			return nil, err
		}
		if err := p.createTablesIfNotExist(); err != nil {
			return nil, err
		}
		if err := p.prepareSqlStatements(); err != nil {
			return nil, err
		}
		return p, nil
	}
}

type pgDb struct {
	dbConn *sql.DB

	sqlSelectBoards *sql.Stmt
	sqlInsertBoard  *sql.Stmt
	sqlSelectBoard  *sql.Stmt
	sqlUpdateBoard  *sql.Stmt
	sqlDeleteBoard  *sql.Stmt

	sqlSelectTasks *sql.Stmt
	sqlInsertTask  *sql.Stmt
	sqlSelectTask  *sql.Stmt
	sqlUpdateTask  *sql.Stmt
	sqlDeleteTask  *sql.Stmt
}

func (p *pgDb) createTablesIfNotExist() error {
	create_sql := `
 		   CREATE TABLE IF NOT EXISTS boards (id SERIAL NOT NULL PRIMARY KEY, name TEXT NOT NULL,
 		   description TEXT NOT NULL);
		   CREATE TABLE IF NOT EXISTS tasks (id SERIAL NOT NULL PRIMARY KEY, name TEXT NOT NULL,
		   description TEXT NOT NULL, status TEXT NOT NULL, bid  INTEGER NOT NULL REFERENCES boards(id));

		`
	if rows, err := p.dbConn.Query(create_sql); err != nil {
		return err
	} else {
		rows.Close()
	}
	return nil
}

func (p *pgDb) prepareSqlStatements() (err error) {
	//Boards
	if p.sqlSelectBoards, err = p.dbConn.Prepare(
		"SELECT id, name, description FROM boards",
	); err != nil {
		return err
	}
	if p.sqlInsertBoard, err = p.dbConn.Prepare(
		"INSERT INTO boards (name, description) VALUES ($1,$2) RETURNING id",
	); err != nil {
		return err
	}
	if p.sqlSelectBoard, err = p.dbConn.Prepare(
		"SELECT id, name, description FROM boards WHERE id = $1",
	); err != nil {
		return err
	}
	if p.sqlUpdateBoard, err = p.dbConn.Prepare(
		"UPDATE boards SET name=$2, description=$3 WHERE id = $1",
	); err != nil {
		return err
	}
	if p.sqlDeleteBoard, err = p.dbConn.Prepare(
		"DELETE FROM boards WHERE id = $1",
	); err != nil {
		return err
	}
	//Tasks
	if p.sqlSelectTasks, err = p.dbConn.Prepare(
		"SELECT id, name, description, status, bid FROM tasks WHERE bid=$1",
	); err != nil {
		return err
	}
	if p.sqlInsertTask, err = p.dbConn.Prepare(
		"INSERT INTO tasks (name, description, status, bid) VALUES ($1,$2,$3,$4) RETURNING id",
	); err != nil {
		return err
	}
	if p.sqlSelectTask, err = p.dbConn.Prepare(
		"SELECT id, name, description, status, bid FROM tasks WHERE id = $1",
	); err != nil {
		return err
	}
	if p.sqlUpdateTask, err = p.dbConn.Prepare(
		"UPDATE tasks SET status=$2 WHERE id = $1",
	); err != nil {
		return err
	}
	if p.sqlDeleteTask, err = p.dbConn.Prepare(
		"DELETE FROM tasks WHERE id = $1",
	); err != nil {
		return err
	}

	return nil
}
