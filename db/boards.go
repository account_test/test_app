package db

import "../model"

func (p *pgDb) SelectBoards() (*[]model.Board, error) {
	boards := make([]model.Board, 0)
	board := model.Board{}
	if res, err := p.sqlSelectBoards.Query(); err != nil {
		return nil, err
	} else {
		for res.Next() {
			err := res.Scan(&board.Id, &board.Name, &board.Description)
			if err != nil {
				return nil, err
			}
			boards = append(boards, board)
		}
	}
	return &boards, nil
}

func (p *pgDb) InsertBoard(b model.Board) (int64, error) {
	var id int64
	err := p.sqlInsertBoard.QueryRow(b.Name, b.Description).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (p *pgDb) SelectBoard(id int) (*model.Board, error) {
	var board model.Board
	row := p.sqlSelectBoard.QueryRow(id)
	if err := row.Scan(&board.Id, &board.Name, &board.Description); err != nil {
		return nil, err
	}
	return &board, nil
}

func (p *pgDb) UpdateBoard(b model.Board) error {
	if _, err := p.sqlUpdateBoard.Exec(b.Id, b.Name, b.Description); err != nil {
		return err
	}
	return nil
}

func (p *pgDb) DeleteBoard(id int) error {
	if _, err := p.sqlDeleteBoard.Exec(id); err != nil {
		return err
	}
	return nil
}
