package db

import "../model"

func (p *pgDb) SelectTasks(bid int) (*[]model.Task, error) {
	tasks := make([]model.Task, 0)
	task := model.Task{}
	if res, err := p.sqlSelectTasks.Query(bid); err != nil {
		return nil, err
	} else {
		for res.Next() {
			err := res.Scan(&task.Id, &task.Name, &task.Description, &task.Status, &task.BoardID)
			if err != nil {
				return nil, err
			}
			tasks = append(tasks, task)
		}
	}
	return &tasks, nil
}

func (p *pgDb) InsertTask(task model.Task) (int64, error) {
	var id int64
	err := p.sqlInsertTask.QueryRow(task.Name, task.Description, task.Status, task.BoardID).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil

}

func (p *pgDb) SelectTask(id int) (*model.Task, error) {
	var task model.Task
	row := p.sqlSelectTask.QueryRow(id)
	if err := row.Scan(&task.Id, &task.Name, &task.Description, &task.Status, &task.BoardID); err != nil {
		return nil, err
	}
	return &task, nil
}

func (p *pgDb) UpdateTask(id int, status string) error {
	if _, err := p.sqlUpdateTask.Exec(id, status); err != nil {
		return err
	}
	return nil
}

func (p *pgDb) RemoveTask(id int) error {
	if _, err := p.sqlDeleteTask.Exec(id); err != nil {
		return err
	}
	return nil
}
