// handlers_test.go
package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"strings"

	"bytes"

	"encoding/json"

	"strconv"

	"./daemon"
	"./model"
	"./rest"
	"github.com/gorilla/mux"
)

var m *model.Model

var taskId string
var boardId string

func init() {
	cfg := daemon.ProcessFlags()
	m = daemon.GetModel(cfg)
}

func TestBoardsInsertHandler(t *testing.T) {
	var jsonStr = []byte(`{"name": "task 1","description": "first task"}"`)
	req, err := http.NewRequest("POST", "/api/v1/boards/add", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("handler returned wrong status code: got %v want %v",
			err, http.StatusOK)
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.Handler(rest.BoardInsertHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	decoder := json.NewDecoder(rr.Body)
	var s map[string]interface{}

	if err := decoder.Decode(&s); err != nil {
		t.Error("handler returned not json response")
	}
	boardId = strconv.Itoa(int(s["id"].(float64)))
	if boardId == "" {
		t.Errorf("handler returned wrong response: got %s",
			rr.Body.String(), rr.Code)
	}
}

func TestBoardsGetHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/boards", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.Handler(rest.BoardsGetHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), "name") {
		t.Errorf("handler returned unexpected code: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

func TestBoardUpdateHandler(t *testing.T) {
	var jsonStr = []byte(`{"name": "in progress"}"`)
	req, err := http.NewRequest("POST", "/api/v1/boards/bid", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId})

	rr := httptest.NewRecorder()
	handler := http.Handler(rest.BoardUpdateHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() == "" {
		t.Errorf("handler returned wrong data: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

func TestBoardGetHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/boards/bid", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId})

	rr := httptest.NewRecorder()
	handler := http.Handler(rest.BoardGetHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() == "" {
		t.Errorf("handler returned wrong data: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

//################### TASKS TESTING ########################

func TestTaskInsertHandler(t *testing.T) {
	var jsonStr = []byte(`{"name": "task 1","description": "first task","status": "new"}"`)
	req, err := http.NewRequest("POST", "/api/v1/boards/bid//task/add", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("handler returned wrong status code: got %v want %v",
			err, http.StatusOK)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId})
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.Handler(rest.TaskInsertHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	decoder := json.NewDecoder(rr.Body)
	var s map[string]interface{}

	if err := decoder.Decode(&s); err != nil {
		t.Error("handler returned not json response")
	}
	taskId = strconv.Itoa(int(s["id"].(float64)))
	if taskId == "" {
		t.Errorf("handler returned wrong response: got %s",
			rr.Body.String(), rr.Code)
	}
}

func TestTaskGetHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/boards/bid//task/id", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId, "tid": taskId})

	rr := httptest.NewRecorder()
	handler := http.Handler(rest.TaskGetHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() == "" {
		t.Errorf("handler returned wrong data: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

func TestTasksGetHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/boards/bid//tasks", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId})
	rr := httptest.NewRecorder()
	handler := http.Handler(rest.TasksGetHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if !strings.Contains(rr.Body.String(), "status") {
		t.Errorf("handler returned unexpected code: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

func TestTaskDeleteHandler(t *testing.T) {
	req, err := http.NewRequest("POST", "/api/v1/boards/bid//task/id//del", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId, "tid": taskId})
	rr := httptest.NewRecorder()
	handler := http.Handler(rest.TaskDeleteHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() != "ok" {
		t.Errorf("handler returned wrong data: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}

func TestBoardDeleteHandler(t *testing.T) {
	req, err := http.NewRequest("POST", "/api/v1/boards/bid//del", nil)
	if err != nil {
		t.Fatal(err)
	}
	req = mux.SetURLVars(req, map[string]string{"bid": boardId})
	rr := httptest.NewRecorder()
	handler := http.Handler(rest.BoardDeleteHandler(m))

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	if rr.Body.String() != "ok" {
		t.Errorf("handler returned wrong data: got %v want %v",
			rr.Body.String(), rr.Code)
	}
}
