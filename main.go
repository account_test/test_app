package main

import (
	"log"

	"./daemon"
)

func init() {

}

func main() {
	cfg := daemon.ProcessFlags()

	if err := daemon.Run(cfg); err != nil {
		log.Printf("Error in main(): %v", err)
	}
}
