# test_app

## To run in docker
### Start container:
> docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=password -d postgres
### Enter the container:
> docker exec -ti postgres /bin/bash
### Start psql:
> psql -U postgres
### Create db testdb:
> create database testdb;
### Exit:
> \q
