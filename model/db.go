package model

type db interface {
	//Boards
	SelectBoards() (*[]Board, error)
	InsertBoard(b Board) (int64, error)
	SelectBoard(id int) (*Board, error)
	UpdateBoard(b Board) error
	DeleteBoard(id int) error
	//Tasks
	SelectTasks(bid int) (*[]Task, error)
	InsertTask(task Task) (int64, error)
	SelectTask(id int) (*Task, error)
	UpdateTask(id int, status string) error
	RemoveTask(id int) error
}
