package model

type Model struct {
	db
}

func New(db db) *Model {
	return &Model{
		db: db,
	}
}

func (m *Model) BoardsSelect() (*[]Board, error) {
	return m.SelectBoards()
}

func (m *Model) BoardInsert(b Board) (int64, error) {
	return m.InsertBoard(b)
}

func (m *Model) BoardSelect(id int) (*Board, error) {
	return m.SelectBoard(id)
}

func (m *Model) BoardUpdate(b Board) error {
	return m.UpdateBoard(b)
}

func (m *Model) BoardRemove(id int) error {
	return m.DeleteBoard(id)
}

func (m Model) TasksSelect(bid int) (*[]Task, error) {
	return m.SelectTasks(bid)
}

func (m *Model) TaskSelect(id int) (*Task, error) {
	return m.SelectTask(id)
}

func (m *Model) TaskInsert(t Task) (int64, error) {
	return m.InsertTask(t)
}

func (m *Model) TaskUpdate(id int, s string) error {
	return m.UpdateTask(id, s)
}

func (m *Model) TaskRemove(id int) error {
	return m.RemoveTask(id)
}
