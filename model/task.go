package model

type Task struct {
	Id          int64  `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Status      string `json:"status"`
	BoardID     int64  `json:"board_id"`
}
