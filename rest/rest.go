package rest

import (
	"net/http"

	"../model"
	"github.com/gorilla/mux"
)

func Start(m *model.Model, listen string) {

	r := mux.NewRouter()
	//Boards
	r.Handle("/api/v1/boards", BoardsGetHandler(m)).Methods("GET")
	r.Handle("/api/v1/boards/add", BoardInsertHandler(m)).Methods("POST")
	r.Handle("/api/v1/boards/bid/{bid}", BoardGetHandler(m)).Methods("GET")
	r.Handle("/api/v1/boards/bid/{bid}", BoardUpdateHandler(m)).Methods("POST")
	r.Handle("/api/v1/boards/bid/{bid}/del", BoardDeleteHandler(m)).Methods("POST")
	//Tasks
	r.Handle("/api/v1/boards/bid/{bid}/tasks", TasksGetHandler(m)).Methods("GET")
	r.Handle("/api/v1/boards/bid/{bid}/task/add", TaskInsertHandler(m)).Methods("POST")
	r.Handle("/api/v1/boards/bid/{bid}/task/id/{tid}", TaskGetHandler(m)).Methods("GET")
	r.Handle("/api/v1/boards/bid/{bid}/task/id/{tid}", TaskUpdateHandler(m)).Methods("POST")
	r.Handle("/api/v1/boards/bid/{bid}/task/id/{tid}/del", TaskDeleteHandler(m)).Methods("POST")

	go http.ListenAndServe(listen, r)
}
